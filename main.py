#!/usr/bin/python3

# main.py
# Main program for the Raspberry Pi Security System

# Location of the configuration file:
confpath = "rpisec.conf"

# Load configuration file into config:
from piSecConfig import import_config
config = import_config(confpath)
# Verify that the configuration is valid:
from piSecConfig import check_config
check_config(config)

# Import sensor-checking functions:
from piSecDevices import door_open
from piSecDevices import motion_detected

# Import camera functions:
import picamera
# Create the camera here to avoid crashes due to resource error:
camera = picamera.PiCamera()

# Initialize the door_is_open variable to avoid errors on first iteration:
door_is_open = False
# By default, no notification has been sent yet:
message_sent = False

# Main program loop:
while True:
    # DOOR-LEFT-OPEN CHECK
    # See if the door has been standing open for a length of time:
    if config['maxdoortime'] != 0:      # Skip this check entirely if it's disabled in config
        door_was_open = door_is_open    # Save previous state of the door
        door_is_open = door_open()      # Get current state of the door
        # If the door is closed, reset the message-sent variable (to prevent email flood):
        if not door_is_open:
            message_sent = False
        # If the door is newly opened, record the current time as the start time:
        if (not door_was_open) and door_is_open:
            import time
            door_opened_time = time.time()
        # Check to see if the specified amount of time has passed:
        if door_is_open:
            try:
                door_opened_time
            except NameError:
                pass
            else:
                # If yes, take action:
                if( time.time() - door_opened_time ) / 60 >= config['maxdoortime']:
                    # If in home mode, sound the alarm:
                    if config['startmode'] == 1:
                        from piSecDevices import alarm
                        alarm(config['alarmpath'])
                    # If in armed mode, send an email:
                    if config['startmode'] == 2:
                        # Don't send a message if it has been done already:
                        if not message_sent:
                            from piSecEmail import notify_noimg
                            notify_noimg(config['smtp'], config['port'], config['fromaddr'], config['frompasswd'], config['toaddr'], "The door is left open!")
                            # Prevent this event from triggering more messages:
                            message_sent = True
                    # Done sending message
                # Done taking action
        # Done checking the passage of time
    # Door check complete for this iteration

    # By default, there is no intruder:
    intruder = False
    # Poll sensors for anything detected:
    if config['startmode'] > 0:
        intruder = door_open()
        if config['startmode'] > 1:
            intruder = intruder or motion_detected()

    # If an intruder is detected, send an alert:
    if intruder:
        # Only send an email alert if in armed mode (2):
        if config['startmode'] > 1:
            # Get the current time as a string:
            import datetime
            now = datetime.datetime.now()
            timestamp = str(now.month) + '-' + str(now.day) + '-' + str(now.year) + '_' + str(now.hour) + str(now.minute)

            # Create the photo's filename:
            photoname = timestamp + '.jpg'
            # Add a delay before the photo is taken:
            import time
            time.sleep(1)
            # Take the photo:
            from piSecDevices import photo
            photo(camera, photoname)

            # Integrate the timestamp into the email notification message:
            message = "An alarm was triggered at " + timestamp + "."

            # Send the notification:
            from piSecEmail import notify
            notify(config['smtp'], config['port'], config['fromaddr'], config['frompasswd'], config['toaddr'], message, photoname)

        # Sound the alarm:
        from piSecDevices import alarm
        alarm(config['alarmpath'])

        # Wait a second to prevent multiple messages from being sent (on account of still-active sensors):
        import time
        time.sleep(1)

# End of main loop
