#!/usr/bin/python3

# piSecConfig.py
# Read settings from the configuration file

# Read data from the config file:
def import_config(_confpath):
    # Open the config file for reading:
    try:
        conf_in = open(_confpath, 'r')
    # If the file fails to open, display an error message and quit
    except OSError:
        print('Error opening configuration in %s' % _confpath)
        quit()

    # Create a dictionary to store the parameters that are entered:
    params = {}

    # Begin interpreting each line in the file:
    for line in conf_in:
        # If a line is a comment, skip it:
        iscomment = False
        for i in range(0, len(line), 1):
            if line[i] == '#':
                iscomment == True
        if iscomment == True:
            continue

        # Split each line at the '=' delimiter:
        parts = line.split('=')

        # If a line does not contain exactly 2 '=' separated parts, skip it:
        if len(parts) != 2:
            continue
        
        # Store the parts in the dictionary, with the first part as the key:
        params[ parts[0] ] = parts[1].replace('\n','')  # Also remove newline characters

    # Return the dictionary containing the parameters:
    return params
# End of import_config

# Check config parameters for completness/correctness:
def check_config(_params):
    # Check each required parameter in the config

    # SMTP server:
    if not 'smtp' in _params.keys():
        print('SMTP server not specified!')
        quit()
    if not 'port' in _params.keys():
        print('SMTP port not specified!')
        quit()
    try:
        int( _params['port'] )
    except ValueError:
        print('Port must be an integer')
        quit()
    else:
        _params['port'] = int( _params['port'] )
    from piSecEmail import check_smtp
    if not check_smtp(_params['smtp'], _params['port']):
        print('Failed to connect to SMTP server.  Check configuration.')
        quit()
    
    # Outgoing email address:
    if not 'fromaddr' in _params.keys():
        print('Outgoing email address not specified!')
        quit()
    
    # Outgoing email password:
    if not 'frompasswd' in _params.keys():
        print('Outgoing email password not specified!')
        quit()
    
    # Destination email address:
    if not 'toaddr' in _params.keys():
        print('Destination email address not specified!')
        quit()
    
    # Startup mode:
    if not 'startmode' in _params.keys():
        print('Startup mode not specified!')
        quit()
    try:
        int( _params['startmode'] )
    except ValueError:
        print('Startup mode must be an integer!')
        quit()
    else:
        _params['startmode'] = int( _params['startmode'] )
    if ( _params['startmode'] > 2 ) or ( _params['startmode'] < 0 ):
        print('Startup mode must be 0, 1, or 2')
        quit()

    # Alarm sound path:
    if not 'alarmpath' in _params.keys():
        print('Alarm sound file not specified!')
        quit()

    # Max door-open time:
    if not 'maxdoortime' in _params.keys():
        print('Maximum door-open time not specified!')
        quit()
    try:
        int( _params['maxdoortime'] )
    except ValueError:
        print('Max door-open time must be an integer!')
        quit()
    else:
        _params['maxdoortime'] = int( _params['maxdoortime'] )
    if _params['maxdoortime'] < 0:
        print('Time must be positive!')
        quit()
