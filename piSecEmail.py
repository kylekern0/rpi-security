#!/usr/bin/python3

# piSecEmail.py
# Defines email functions used by the security system

import smtplib # Use standard email functions

# Include other functions necessary for sending images:
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

# Send mail from _fromaddr on server _fromsmtp to _toaddr containing message _message and the image at _imagepath:
def notify(_fromsmtp, _fromsmtpport, _fromaddr, _frompasswd, _toaddr, _message, _imagepath):
    # Object to store the email to be sent, including images:
    msg = MIMEMultipart()
    msg['From'] = _fromaddr
    msg['To'] = _toaddr
    msg['Subject'] = 'Pi Security Event'
    msg.attach(MIMEText(_message, 'plain')) # Convert message to a format compatible with the msg object

    attachmentfile = open(_imagepath, 'rb')
    attachment = MIMEImage(attachmentfile.read())
    attachmentfile.close()
    attachment.add_header('Content-ID', '<{}>'.format(_imagepath))
    msg.attach(attachment)

    # Connect to the server _fromsmtp on port _fromsmtpport:
    server = smtplib.SMTP(_fromsmtp, _fromsmtpport)
    # Connect using TLS:
    server.starttls()
    # Login to the Pi's email address:
    server.login(_fromaddr, _frompasswd)
    
    # Convert the message to text:
    text = msg.as_string()
    # Send the message:
    server.sendmail(_fromaddr, _toaddr, text)
    # Disconnect:
    server.quit()
# End of notify

# Same as notify(), but without an image attachment (for use with open door alerts):
def notify_noimg(_fromsmtp, _fromsmtpport, _fromaddr, _frompasswd, _toaddr, _message):
    # Object to store the email to be sent, including images:
    msg = MIMEMultipart()
    msg['From'] = _fromaddr
    msg['To'] = _toaddr
    msg['Subject'] = 'Pi Security Event'
    msg.attach(MIMEText(_message, 'plain')) # Convert message to a format compatible with the msg object

    # Connect to the server _fromsmtp on port _fromsmtpport:
    server = smtplib.SMTP(_fromsmtp, _fromsmtpport)
    # Connect using TLS:
    server.starttls()
    # Login to the Pi's email address:
    server.login(_fromaddr, _frompasswd)
    
    # Convert the message to text:
    text = msg.as_string()
    # Send the message:
    server.sendmail(_fromaddr, _toaddr, text)
    # Disconnect:
    server.quit()
# End of notify_noimg

# EMAIL VERIFICATION FUNCTIONS
# Used to verify that email parameters from the config files are valid

# Check that an SMTP server and its associated port can be connected:
# Returns False if the server can't connect, returns True otherwise
def check_smtp(_addr, _port):
    try:
        smtplib.SMTP(_addr, _port)
    except smtplib.SMTPConnectError:
        return False
    else:
        return True
# End of check_smtp

# Check that an email address has the correct format:
# Returns False if the email address isn't valid, returns True otherwise
def check_address(_addr):
    # Split the address into two parts at the @ character
    addr_parts = _addr.split('@')
    # Make sure there are only two parts - the local part and the remote part:
    if len(addr_parts) != 2:
        return False
    
    # Make sure that the local part is valid:
    # Check the length:
    if not 0 < len(addr_parts[0]) <= 64:
        return False
    
    # Make sure that the remote part is valid:
    # Check for at least one '.':
    contains_dot = False
    for i in range(0, len(addr_parts[1], 1)):
        if addr_parts[1][i] == '.':
            contains_dot == True
    if not contains_dot:
        return False
    
    # If the address survived those checks, it is valid (as far as we know)
    return True
# End of check_address
