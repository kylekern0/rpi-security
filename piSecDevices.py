#!/usr/bin/python3

# piSecDevices.py
# Contains functions for accessing sensors and other physical devices attached to the system

# Variables to store sensor GPIO pin numbers:
door_pin = 7
pir_pin = 11

# Import necessary functions:
import RPi.GPIO as GPIO     # For accessing RPi GPIO header
import picamera             # For taking photos
import time                 # For setting timers
import pygame               # For audio

# Set up the GPIO header for use with the security system:
GPIO.setwarnings(0)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pir_pin, GPIO.IN)      # Input from PIR sensor on GPIO 11
GPIO.setup(door_pin, GPIO.IN)     # Input from door switch on GPIO 7

# SENSOR-CHECKING FUNCTIONS HERE:

# Returns True if the door is open, False if not:
def door_open():
    if GPIO.input(door_pin) == 1:
        return True
    else:
        return False
# End of door_open()

# Returns True if motion is detected, False if not:
def motion_detected():
    if GPIO.input(pir_pin) == 1:
        return True
    else:
        return False
# End of motion_detected()

# Play the loaded alarm sound:
def alarm(music_path):
    pygame.mixer.init()
    pygame.mixer.music.load(music_path)
    pygame.mixer.music.play()
# End of alarm()

# Take a photo with the camera and save it with the file name
def photo(_camera, _filename):
    _camera.capture(_filename)
# End of photo()