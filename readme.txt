Modular Raspberry Pi Security System
Developed by Kyle Kern and Miles Hanna

This is a home security system which detects intruders using a door switch and a passive infrared sensor, then takes a photo and sends it to the homeowner's email address.

HARDWARE SETUP
  * Connect the output from the door switch to GPIO pin 7
  * Connect the output from the PIR sensor to GPIO pin 11
  * Connect the Raspberry Pi Camera to the header on the Raspberry Pi via the ribbon cable

SOFTWARE SETUP
  This system requires a GNU/Linux operating system with the following dependencies:
    * Python 3
    * Raspberry Pi GPIO Python 3 libraries
    * Raspberry Pi Camera Python 3 libraries
    * Python 3 email libraries
    * Python 3 pygame library

CONFIGURATION
  The configuration options for the security system are located and documented in rpisec.conf

LAUNCHING
  From the directory containing main.py:
    Set the executable attribute on the file main.py:
      $ chmod +x main.py
    Start the system with:
      $ ./main.py